import UserInterface from './UserInterface';

export default class User implements UserInterface {

    id: Number;

    name: String;
    
    address: String;
    
    email: String;
    
    age: Number;

    constructor(user: UserInterface){
        this.id = user.id;
        this.name = user.name;
        this.address = user.address;
        this.email = user.email;
        this.age = user.age;
    }

}