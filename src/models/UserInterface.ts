export default interface UserInterface {

    id: Number;

    name: String;
    
    address: String;
    
    email: String;
    
    age: Number;

}