import axios from 'axios';
import User from '../models/User';
import config from '../config';
import UserInterface from '@/models/UserInterface';

export default class UsersRepository {

    public users: Array<User> = new Array<User>();

    public static async getUsers (): Promise<Array<UserInterface>>{
        let response = await axios.get(config.SERVER.concat('/users'));
        return response.data;
    }

    public addUser (user: UserInterface) {
        this.users.push(new User(user));
    }

}