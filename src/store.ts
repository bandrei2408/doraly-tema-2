import Vue from 'vue';
import Vuex from 'vuex';
import User from './models/User'
import axios from 'axios'
import UsersRepository from '@/repositories/UsersRepository';
import UserInterface from '@/models/UserInterface';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    usersRepository: new UsersRepository()
  },
  mutations: {
    ADD_USER: ({ usersRepository }, user: UserInterface) => {
      usersRepository.addUser(user);
    },
    REPLACE_USER: ({ usersRepository }, user: User) => {
      
    },
    REMOVE_USER: ({ usersRepository }, id: Number) => {

    }
  },
  actions: {
    GET_USERS: ({ commit }) => {
      UsersRepository.getUsers().then((users: Array<UserInterface>) => {
        users.forEach((user: UserInterface) => {
          commit("ADD_USER", user);
        });
      });
    }
  },
  getters: {
    users: ({ usersRepository }) => {
      return usersRepository.users;
    }
  }
});
